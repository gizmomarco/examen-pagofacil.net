<?php

namespace Application\UsuarioBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\UsuarioBundle\Entity\Usuario;

class Usuarios extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 1;
    }

    public function load(ObjectManager $manager) {
        $usuarios = array(
            array('usuario' => 'root', 'pwd' => 'admin', 'nombre' => 'admin', 'email' => 'marco.mendez.beltran@gmail.com', 'estatus' => '1'),
        );
        foreach ($usuarios as $usuario) {
            $entidad = new Usuario();
            $entidad->setUsuario($usuario['usuario']);
            $entidad->setPassword($usuario['pwd']);
            $entidad->setNombre($usuario['nombre']);
            $entidad->setEmail($usuario['email']);
            $entidad->setEstatus($usuario['estatus']);
            $manager->persist($entidad);
        }
        $manager->flush();
    }

}
