<?php

namespace Application\UsuarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Application\UsuarioBundle\Form\CuentaType;
use Application\UsuarioBundle\Form\ContrasenaType;
use Application\UsuarioBundle\Entity\Token;

class DefaultController extends Controller {

    /**
     * loginAction
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Controller
     */
    public function loginAction(Request $request) {
        $session = $request->getSession();
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContextInterface::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);
        return $this->render(
                        'login.html.twig', array(
                    'last_username' => $lastUsername,
                    'error' => $error,
                        )
        );
    }

    /**
     * indexAction
     * @return \Controller
     */
    public function indexAction(Request $request) {
        if ($this->get('security.context')->getToken()) {
            return $this->render('base.html.twig', array(
                        'nombre' => $this->get('security.context')->getToken()->getUser()->getNombre()
            ));
        } else {
            return $this->redirect($this->generateUrl('admin'));
        }
    }

    /**
     * logoutAction
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Controller
     */
    public function logoutAction(Request $request) {
        $session = $request->getSession();
        $session->invalidate();
        return $this->redirect($this->generateUrl('login'));
    }

    public function miCuentaEditarAction() {
        $form = $this->createCuentaForm();
        return $this->render('mi-cuenta.html.twig', array(
                    'form' => $form->createView(),
                    'titulo' => ' Mi cuenta ',
                    'subtitulo' => ' Editar ',
        ));
    }

    public function miCuentaActualizarAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UsuarioBundle:Usuario')->find($this->get('security.context')->getToken()->getUser()->getId());
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }
        $editForm = $this->createCuentaForm($entity);
        $editForm->handleRequest($request);
        $mensaje = '';
        if ($editForm->isValid()) {
            $em->flush();
            $mensaje = 'La información se ha actualizado.';
        }
        return $this->render('mi-cuenta.html.twig', array(
                    'form' => $editForm->createView(),
                    'titulo' => ' Mi cuenta ',
                    'subtitulo' => ' Editar ',
                    'mensaje' => $mensaje,
        ));
    }

    private function createCuentaForm() {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UsuarioBundle:Usuario')->find($this->get('security.context')->getToken()->getUser()->getId());
        $form = $this->createForm(new CuentaType(), $entity, array(
            'action' => $this->generateUrl('mi-cuenta-actualizar'),
            'method' => 'PUT',
        ));
        $form->add('save', 'submit', array('attr' => array('class' => 'btn btn-green')));
        $form->add('back', 'button', array('attr' => array('class' => 'btn btn-danger goto')));
        return $form;
    }

    public function miContrasenaEditarAction() {
        $form = $this->createContrasenaForm();
        return $this->render('mi-contrasena.html.twig', array(
                    'form' => $form->createView(),
                    'titulo' => ' Mi contraseña ',
                    'subtitulo' => ' Editar ',
        ));
    }

    public function miContrasenaActualizarAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UsuarioBundle:Usuario')->find($this->get('security.context')->getToken()->getUser()->getId());
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }
        $editForm = $this->createContrasenaForm($entity);
        $editForm->handleRequest($request);
        $mensaje = '';
        if ($editForm->isValid()) {
            $em->flush();
            $mensaje = '';
        }
        return $this->render('mi-contrasena.html.twig', array(
                    'form' => $editForm->createView(),
                    'titulo' => ' Mi contraseña ',
                    'subtitulo' => ' Editar ',
                    'mensaje' => 'La contraseña se ha actualizado.',
        ));
    }

    private function createContrasenaForm() {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UsuarioBundle:Usuario')->find($this->get('security.context')->getToken()->getUser()->getId());
        $form = $this->createForm(new ContrasenaType(), $entity, array(
            'action' => $this->generateUrl('mi-contrasena-actualizar'),
            'method' => 'PUT',
        ));
        $form->add('save', 'submit', array('attr' => array('class' => 'btn btn-green')));
        $form->add('back', 'button', array('attr' => array('class' => 'btn btn-danger goto')));
        return $form;
    }

    private function createCambiarContrasenaForm($usuario) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UsuarioBundle:Usuario')->find($usuario);
        $form = $this->createForm(new ContrasenaType(), $entity, array(
            'action' => $this->generateUrl('mi-contrasena-guardar', array('usuario' => $entity->getId())),
            'method' => 'PUT',
        ));
        $form
                ->add('send', 'submit', array('attr' => array('class' => 'btn btn-green')))
                ->add('back', 'button', array('attr' => array('class' => 'btn btn-danger goto')));
        return $form;
    }

    private function createEmailForm() {
        $form = $this->createFormBuilder()
                ->add('email', 'email', array('required' => TRUE, 'label' => ' Correo electrónico ', 'label_attr' => array('class' => 'col-sm-2 control-label'), 'attr' => array('class' => 'form-control')))
                ->setAction($this->generateUrl('mi-contrasena-enviar'))
                ->add('send', 'submit', array('attr' => array('class' => 'btn btn-bricky pull-right')))
                ->add('back', 'button', array('attr' => array('class' => 'btn btn-green goto')));
        return $form;
    }

    public function miContrasenaRecuperarAction() {
        $form = $this->createEmailForm()->getForm();
        return $this->render('mi-contrasena-recuperar.html.twig', array(
                    'form' => $form->createView(),
                    'titulo' => ' Mi contraseña ',
                    'subtitulo' => ' Recuperar ',
        ));
    }

    public function miContrasenaEnviarAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createEmailForm()->getForm();
        $form->handleRequest($request);
        $respuesta = '';
        $clase = '';
        $icon = '';
        if ($form->isValid()) {
            $usuario = $em->getRepository('UsuarioBundle:Usuario')->findOneByEmail($request->request->get('form')['email']);
            if ($usuario != null) {
                $token = new Token();
                $token->setCadena(sha1(md5(base64_encode(date('Y-m-s H:i:s ') . rand(1000000000000, 9999999999999) . ' ' . $usuario->getNombre()))));
                $token->setUsuario($usuario);
                $token->setCaducidad(new \DateTime(date("Y-m-d H:i:s", strtotime("now + 4 hours"))));
                $em->persist($token);
                $em->flush();
                $mensaje = $this->renderView('enviar.html.twig', array(
                    'token' => $token->getCadena(),
                    'usuario' => $usuario,
                ));
                $mailer = $this->get('mailer');
                $message = $mailer->createMessage()
                        ->setSubject('Cambio de contraseña')
                        ->setFrom(array('info@hotmail.com' => 'Aviso sistemas'))
                        ->setTo($usuario->getEmail())
                        ->setBody($mensaje, 'text/html');
                $mailer->send($message);
                $respuesta = 'Revise su bandeja de correo electrónico.';
                $clase = 'success';
                $icon = 'fa-check-circle';
            } else {
                $respuesta = 'Se encontraron los siguientes errores: Correo electrónico no registrado.';
                $clase = 'warning';
                $icon = 'fa-exclamation-triangle';
            }
        } else {
            $respuesta = 'Se encontraron los siguientes errores:';
            $clase = 'danger';
            $icon = 'fa-times-circle';
        }
        return $this->render('mi-contrasena-recuperar.html.twig', array(
                    'form' => $form->createView(),
                    'titulo' => ' Mi contraseña ',
                    'subtitulo' => ' Recuperar ',
                    'respuesta' => $respuesta,
                    'clase' => $clase,
                    'icon' => $icon,
        ));
    }

    public function miContrasenaCambiarAction($cadena) {
        $em = $this->getDoctrine()->getManager();
        $token = $em->getRepository('UsuarioBundle:Token')->findOneByCadena($cadena);
        $respuesta = '';
        $clase = '';
        $icon = '';
        if ($token != null) {
            $form = $this->createCambiarContrasenaForm($token->getUsuario()->getId());
            return $this->render('mi-contrasena-cambiar.html.twig', array(
                        'form' => $form->createView(),
                        'respuesta' => $respuesta,
                        'clase' => $clase,
                        'icon' => $icon,
            ));
        } else {
            return $this->redirect($this->generateUrl('login'));
        }
    }

    public function miContrasenaGuardarAction(Request $request, $usuario) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UsuarioBundle:usuario')->find($usuario);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }
        $form = $this->createCambiarContrasenaForm($entity);
        $form->handleRequest($request);
        $respuesta = '';
        $clase = '';
        $icon = '';
        if ($form->isValid()) {
            $tokens = $em->getRepository('UsuarioBundle:Token')->findByUsuario($entity);
            foreach ($tokens as $token) {
                $em->remove($token);
            }
            $em->flush();
            $respuesta = 'Contraseña modificada exitosamente.';
            $clase = 'success';
            $icon = 'fa-check-circle';
        } else {
            $respuesta = 'La petición tiene errores.';
            $clase = 'danger';
            $icon = 'fa-time-circle';
        }
        return $this->render('mi-contrasena-cambiar.html.twig', array(
                    'form' => $form->createView(),
                    'respuesta' => $respuesta,
                    'clase' => $clase,
                    'icon' => $icon,
        ));
    }

}
