<?php

namespace Application\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CuentaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array('required' => TRUE, 'label' => ' Nombre ', 'label_attr' => array('class' => 'col-sm-2 control-label'), 'attr' => array('class' => 'form-control')))
            ->add('email', 'email', array('required' => TRUE, 'label' => ' Correo electrónico ', 'label_attr' => array('class' => 'col-sm-2 control-label'), 'attr' => array('class' => 'form-control')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Application\UsuarioBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'application_usuariobundle_cuenta';
    }
}
