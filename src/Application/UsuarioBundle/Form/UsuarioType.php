<?php

namespace Application\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;

class UsuarioType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usuario', 'text', array('label' => ' Usuario ', 'label_attr' => array('class' => 'col-sm-2 control-label'), 'required' => TRUE, 'attr' => array('class' => 'form-control')))
            ->add('nombre', 'text', array('label' => ' Nombre completo ', 'label_attr' => array('class' => 'col-sm-2 control-label'), 'required' => TRUE, 'attr' => array('class' => 'form-control')))
            ->add('email', 'email', array('label' => ' Correo electrónico ', 'label_attr' => array('class' => 'col-sm-2 control-label'), 'required' => TRUE, 'attr' => array('class' => 'form-control')))
            ->add('estatus', 'choice', array('label' => ' Estatus ', 'label_attr' => array('class' => 'col-sm-2 control-label'), 'choice_list' => new ChoiceList(array(1, 0), array('Activo', 'Inactivo')), 'required' => TRUE, 'attr' => array('class' => 'form-control')))
            ->add('save', 'submit', array('attr' => array('class' => 'btn btn-green')))
            ->add('back', 'button', array('attr' => array('class' => 'btn btn-danger goto')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Application\UsuarioBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'application_usuariobundle_usuario';
    }
}
