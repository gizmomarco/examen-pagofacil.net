# Instalación #

## Consideraciones previas ##

Se deberá contar con Composer instalado (**https://getcomposer.org**).

Descargar el repositorio en la carpeta de proyectos web.

## Instalación de Symfony 2 ##

Ejecutar el comando **composer install** desde la ubicación donde se descargó el proyecto

## Operaciones básicas con la base de datos ##

Crear la BBDD en MySql, con su usuario y los permisos necesarios para acceder y modificar la misma. Hecho lo anterior, se deberán registrar los datos de acceso a la BBDD en el archivo **/app/config/parameters.yml**

Cargar la primera tabla al proyecto a través del comando **php app/console doctrine:schema:update --force**

Cargar los fixtures de dicha tabla al proyecto, ejecutar el comando **php app/console doctrine:fixtures:load**


**Usuario**: root

**Contraseña**: admin