var Dropzone = function () {
    var runDropzone = function () {
        $("#my-awesome-dropzone").dropzone({
            paramName: "file", 
            maxFilesize: 5.0, 
            addRemoveLinks: true,
            maxFiles: 1,
            acceptedFiles: 'image/*',
            accept: function(file, done) {
                console.log("uploaded");
                done();
            },
            addRemoveLinks: false,
            success: function(file, response){
                var respuesta = response.split("|");
                $("#"+respuesta[0]).val(respuesta[1]);
                return file.previewElement.classList.add("dz-success");
            },
            init: function() {
                this.on("addedfile", function() {
                    if (this.files[1]!=null){
                        this.removeFile(this.files[0]);
                    }
                });
            }
        });
        $("#my-awesome-dropzone-2").dropzone({
            paramName: "file", 
            maxFilesize: 5.0, 
            addRemoveLinks: false,
            maxFiles: 5,
            uploadMultiple: true,
            acceptedFiles: 'image/*',
            accept: function(file, done) {
                done();
            },
            success: function(file, response){
                $("#galeria").append(response);
                this.removeFile(file);
                return file.previewElement.classList.add("dz-success");
            },
            init: function() {
            }
        });
    };
    return {
        init: function () {
            runDropzone();
        }
    };
}();